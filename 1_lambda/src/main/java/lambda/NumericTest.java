package lambda;

public interface NumericTest {
    boolean examine(int n);
}