import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DemoSort {
    public static void main(String[] args) {
        String[] stringArray = {"Charlie", "Beta", "Alfa", "Delta", "Echo", "Foxtrot"};
        List<String> myList = new ArrayList<>(Arrays.asList(stringArray));

        System.out.println("alfabetisch:");
        //TODO
        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nvolgens lengte:");
        //TODO
        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nomgekeerd alfabetisch:");
        //TODO
        for (String s : myList) {
            System.out.print(s + " ");
        }

        System.out.println("\nvolgens laatste karakter:");
        //TODO
        for (String s : myList) {
            System.out.print(s + " ");
        }

    }
}
